Caixa CNAB240 

Path pode ser usado para corrigir qualquer remessa caixa CNAB 240 que tenha o mesmo problema.

Por padrão a data da multa e do juros devem serem um dia após o vencimento.

Exemp:
Vencimento => 10/01/2018
Data Juros => 11/01/2018
Data Multa => 11/01/2018

Conteúdo atual da remessa:
Vencimento => 10/01/2018
Data Juros => 11/01/2018
Data Multa => 10/01/2018

Problema => Data da multa é a mesma do vencimento.

Conteúdo corrigido
Vencimento => 10/01/2018
Data Juros => 11/01/2018
Data Multa => 11/01/2018 => Sistema localiza a data do Juros a tribui


obs: path basicamente define a data do multa como sendo a mesma do juros, se a data do juros estiver errada logicamente 
a da multa também ficará.

PARA QUE TENHA EXIDO É NECESSÁRIO QUE O EXECUTABEL ESTEJÁ N MESMO DIRETÓRIO DO ARQUIVO, E A NOMECLATURA DA REMESSA SIGA O SEGUINTE PADRÃO

dia/mês/ano => 01032018.REM